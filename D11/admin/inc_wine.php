<?php 
require_once './inc_func.php';
require_once './dbHelper.php';

$flag = false;
 
 if(isset($_POST["btnThem"]))
 {
 	$WineName = $_POST["WineName"];
 	$sql ="INSERT into  wine (WineName) values('".$WineName."')";
 	save($sql, 0);
 }
 if(isset($_POST["btnXoa"]))
 {
 	$wiID = $_POST["wiID"];
 	$sql = "DELETE  from wine where wiID =".$wiID;
 	save($sql,1);
 }

 if(isset($_POST["btnSua"]))
 {
 	$wiID = $_POST["wiID"];
 	$WineName = $_POST["WineName"];
 	$sql = "update wine set WineName = '".$WineName."' where wiID = ".$wiID;
 	save($sql,1);
 }
$_wiID = 0;
$_WineName="";
if(isset($_GET["wiID"]))
{
	$flag = true;
	$_wiID= $_GET["wiID"];
	$sql = "select * from wine where wiID = ".$_wiID;
	$list = load($sql);
	$row = $list->fetch_assoc();


	$_wiID = $row["wiID"];
	$_WineName = $row["WineName"];
}



?>


<div class="col-md-9">
	<div class="panel panel-default">
		<div class="panel-heading">
			<h3 class="panel-title">Admin</h3>
		</div>
		<div class="panel-body">
			<table class="table table-hover" border="0">
				<thead>
					<tr class="bg-info">
						<th>STT</th>
						<th>Tên Loại vang</th>
						<th>Chức năng</th>
					</tr>
				</thead>
				<tbody>
					<?php
					$sql = "SELECT * FROM wine";
					$rs = load($sql);
					while($row = $rs->fetch_assoc())
					{
						?>
						<tr>
							<td><?php echo $row["wiID"];?></td>			
							<td><?php echo $row["WineName"];?></td>
							<td>
								<a class="btn btn-primary" href="?act=wine1&wiID=<?php echo $row["wiID"];?>">Chọn</a>
							</td>
						</tr>
						<?php
					}
					?>
					
				</tbody>
			</table>
			<hr>
			<!-- phần thêm xóa sửa -->
			<form class="form-horizontal" method="post" action="?act=wine1" id="addProductForm" enctype="multipart/form-data">

				<input type="hidden" class="form-control" id="txtProID" name="wiID" value="<?php if($flag) echo $_wiID?>" />

				<div class="form-group">
					<label for="txtProName" class="col-sm-2 control-label">Sản phẩm:</label>
					<div class="col-sm-4">
						<input type="text" class="form-control" id="txtProName" name="WineName" value="<?php if($flag) echo $_WineName?>" />
					</div>
				</div>
				<div class="col-sm-10 col-sm-offset-2">
					<?php if(!$flag) {?>
					<button type="submit" class="btn btn-primary" name="btnThem">
						<i class="fa fa-plus"></i>&nbsp;Thêm sản phẩm
					</button>
					<?php }else{ ?>
					<button type="submit" class="btn btn-success" name="btnSua">
						<i class="fa fa-edit"></i>&nbsp;Sửa sản phẩm
					</button>
					<button type="submit" class="btn btn-danger" name="btnXoa">
						<i class="fa fa-trash"></i>&nbsp;Xóa sản phẩm
					</button>
					
					<?php }?>
				</div>
			</form>


		</div>
	</div>
</div>
<?php
$js = <<<JS
	<script src="assets/bootstrap-select/js/bootstrap-select.min.js"></script>
 <script src="assets/bootstrap-touchspin/jquery.bootstrap-touchspin.min.js"></script>
  <script src="assets/tinymce/tinymce.min.js"></script>
    <script src="assets/bootstrap.file-input.js"></script>
    <script src="assets/jquery-validation-1.15.0/jquery.validate.min.js"></script>
   <script type="text/javascript">
        $('#txtPrice').TouchSpin({
            min: 0,
            max: 9999999,
            step: 10000,
            verticalbuttons: true,
        });
        $('#txtQ').TouchSpin({
            min: 1,
            verticalbuttons: true,
        });
        $('.selectpicker').selectpicker();

        tinymce.init({
            selector: 'textarea',
            menubar: false,
            toolbar1: 'styleselect | bold italic | link image | alignleft aligncenter',
            toolbar2: 'fontselect | fontsizeselect | forecolor backcolor',
            plugins: 'link image textcolor',
            encoding: 'xml',
        });
        $('input[type = file]').bootstrapFileInput();

        $.validator.addMethod("imageOnly", function (value, element) {
            return this.optional(element) || /^.+\.(jpg|JPG|png|PNG)$/.test(value);
        });
        $('#addProductForm').validate({
            rules: {
                WineName: {
                    required: true
                },
                CatID:{
					required: true	
                },
                fuMain: {
                    imageOnly: true
                },
                fuThumbs: {
                    imageOnly: true
                }
            },
            messages: {
                WineName: {
                    required: "Chưa nhập tên sản phẩm",
                },
                 CatID: {
                    required: "Chưa nhập rượu vang",
                },
                fuMain: {
                    imageOnly: "Hình lớn: chỉ chấp nhận file ảnh sản phẩm."
                },
                fuThumbs: {
                    imageOnly: "Hình nhỏ: chỉ chấp nhận file ảnh sản phẩm."
                }
            },
            errorElement: 'span',
            errorClass: 'help-block',

            highlight: function (element) {
                $(element)
                .closest('.form-group').addClass('has-error');
            },
            success: function (label) {
                label.closest('.form-group').removeClass('has-error');
                label.remove();
            },
        });

        function clearSeachBox() {
            $('#query'.val(''));
            $('#query'.focus(''));
        }
    </script>
JS;

 