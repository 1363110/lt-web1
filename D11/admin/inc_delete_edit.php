<?php
require_once './inc_func.php';
require_once './dbHelper.php';

$_ProID = 0;
$_ProName="";
$_CatID=0;
$_Price= 0;
$_TinyDes="";
$_Quantity=0;
$_FullDes="";
$wiID=0;
if(isset($_GET["ProID"]))
{

	$_ProID = $_GET["ProID"];
	$sql = "select * from products where ProID = ".$_ProID;
	$list = load($sql);
	$row = $list->fetch_assoc();


	$_ProID = $row["ProID"];
	$_ProName = $row["ProName"];
	$_CatID = $row["CatID"];
	$_Price = $row["Price"];
	$_TinyDes = $row["TinyDes"];
	$_Quantity = $row["Quantity"];
	$_FullDes = $row["FullDes"];
	$_wiID = $row["wiID"];
}
?>
<div class="col-md-9">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Panel title</h3>
        </div>
        <div class="panel-body">
            <form class="form-horizontal" method="post" action="?act=admin" id="addProductForm" enctype="multipart/form-data">

				<input type="hidden" class="form-control" id="txtProID" name="ProID" value="<?php echo $_ProID?>" />

				<div class="form-group">
					<label for="txtProName" class="col-sm-2 control-label">Sản phẩm:</label>
					<div class="col-sm-4">
						<input type="text" class="form-control" id="txtProName" name="ProName" value="<?php echo $_ProName?>" />
					</div>
					<label for="CatID" class="col-sm-2 control-label">Danh mục:</label>
					<div class="col-sm-4">
						<select class="form-control selectpicker" name="CatID">
						<option value="0">Chọn Rượu vang</option>
							<?php
							$sql = "select * from categories";
							$rs = load($sql);
							while($row = $rs->fetch_assoc())
							{
								?>
								<option <?php if($_CatID == $row["CatID"]) echo"selected";?> value="<?php echo $row["CatID"];?>"><?php echo $row["CatName"];?></option>
								<?php
							}
						?>
							</select>
						</div>      
					</div>
					<div class="form-group">
						<label for="HangID" class="col-sm-2 control-label">Hãng</label>
						<div class="col-sm-10">
							<select class="form-control selectpicker" name="HangID">					
								<option value="0">Chọn Rượu vang</option>
							<?php
							$sql = "select * from wine";
							$rs = load($sql);
							while($row = $rs->fetch_assoc())
							{
								?>
								<option <?php if($_wiID == $row["wiID"]) echo"selected";?> value="<?php echo $row["wiID"];?>"><?php echo $row["WineName"];?></option>
								<?php
							}
						?>
							}
						</select>
					</div>
				</div>
				<div class="form-group">
					<label for="txtProName" class="col-sm-2 control-label">Giá:</label>
					<div class="col-sm-4">
						<input type="text" class="form-control" id="txtPrice" name="Price"  value="<?php echo $_Price?>" />
					</div>
					<label for="txtQ" class="col-sm-2 control-label">Số lượng:</label>
					<div class="col-sm-4">
						<input type="text" class="form-control" id="txtQ" name="Quantity" value="<?php  echo $_Quantity?>" />
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label">Hình lớn:</label>
					<div class="col-sm-4">
						<input type="file" id="fuMain" name="fuMain" data-filename-placement="inside" />
					</div>
					<label class="col-sm-2 control-label">Hình nhỏ:</label>
					<div class="col-sm-4">
						<input type="file" id="fuThumbs" name="fuThumbs" @*data-filename-placement="inside"*@ />
					</div>
				</div>
				<div class="form-group">
					<label for="txtQ" class="col-sm-2 control-label">Mô tả Ngắn:</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" id="txtTinyDes" name="TinyDes" value="<?php echo $_TinyDes?>" />
					</div>
				</div>
				<div class="form-group">
					<label for="txtQ" class="col-sm-2 control-label">Chi tiết:</label>
					<div class="col-sm-10 col-sm-offset-2">
						<textarea class="form-control" id="txtFullDes" name="FullDes" cols="5" rows="10">
							<?php echo $_FullDes?>
						</textarea>
					</div>
				</div>
				<div class="col-sm-10 col-sm-offset-2">
							
					<button type="submit" class="btn btn-danger" name="btnXoa">
						<i class="fa fa-check"></i>&nbsp;Xóa
					</button>
					<button type="submit" class="btn btn-danger" name="btnSua">
						<i class="fa fa-check"></i>&nbsp;Sửa
					</button>
					<a href="?act=admin" class="btn btn-success">
						<i class="fa fa-reply"></i>&nbsp;Danh sách sản phẩm
					</a>
				</div>
			</form>
        </div>
    </div>
</div>