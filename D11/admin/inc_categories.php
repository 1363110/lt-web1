<?php
require_once './inc_func.php';
require_once './dbHelper.php';

$flag = false;
 
 if(isset($_POST["btnThem"]))
 {
 	$CatName = $_POST["CatName"];
 	$sql ="INSERT into  categories (CatName) values('".$CatName."')";
 	save($sql, 0);
 }
 if(isset($_POST["btnXoa"]))
 {
 	$CatID = $_POST["CatID"];
 	$sql = "DELETE  from categories where CatID =".$CatID;
 	save($sql,1);
 }

 if(isset($_POST["btnSua"]))
 {
 	$CatID = $_POST["CatID"];
 	$CatName = $_POST["CatName"];
 	$sql = "update categories set CatName = '".$CatName."' where CatID = ".$CatID;
 	save($sql,1);
 }
$_CatID = 0;
$_CatName="";
if(isset($_GET["CatID"]))
{
	$flag = true;
	$_CatID= $_GET["CatID"];
	$sql = "select * from categories where CatID = ".$_CatID;
	$list = load($sql);
	$row = $list->fetch_assoc();


	$_CatID = $row["CatID"];
	$_CatName = $row["CatName"];
}



?>


<div class="col-md-9">
	<div class="panel panel-default">
		<div class="panel-heading">
			<h3 class="panel-title">Admin</h3>
		</div>
		<div class="panel-body">
			<table class="table table-hover" border="0">
				<thead>
					<tr class="bg-info">
						<th>STT</th>
						<th>Tên Danh mục</th>
						<th>Chức năng</th>
					</tr>
				</thead>
				<tbody>
					<?php
					$sql = "SELECT * FROM categories";
					$rs = load($sql);
					while($row = $rs->fetch_assoc())
					{
						?>
						<tr>
							<td><?php echo $row["CatID"];?></td>			
							<td><?php echo $row["CatName"];?></td>
							<td>
								<a class="btn btn-primary" href="?act=category&CatID=<?php echo $row["CatID"];?>">Chọn</a>
							</td>
						</tr>
						<?php
					}
					?>
					
				</tbody>
			</table>
			<hr>
			<!-- phần thêm xóa sửa -->
			<form class="form-horizontal" method="post" action="?act=category" id="addProductForm" enctype="multipart/form-data">

				<input type="hidden" class="form-control" id="txtProID" name="CatID" value="<?php if($flag) echo $_CatID?>" />

				<div class="form-group">
					<label for="txtProName" class="col-sm-2 control-label">Sản phẩm:</label>
					<div class="col-sm-4">
						<input type="text" class="form-control" id="txtProName" name="CatName" value="<?php if($flag) echo $_CatName?>" />
					</div>
				</div>
				<div class="col-sm-10 col-sm-offset-2">
					<?php if(!$flag) {?>
					<button type="submit" class="btn btn-primary" name="btnThem">
						<i class="fa fa-plus"></i>&nbsp;Thêm sản phẩm
					</button>
					<?php }else{ ?>
					<button type="submit" class="btn btn-success" name="btnSua">
						<i class="fa fa-edit"></i>&nbsp;Sửa sản phẩm
					</button>
					<button type="submit" class="btn btn-danger" name="btnXoa">
						<i class="fa fa-trash"></i>&nbsp;Xóa sản phẩm
					</button>
					
					<?php }?>
				</div>
			</form>


		</div>
	</div>
</div>
<?php
$js = <<<JS
	<script src="assets/bootstrap-select/js/bootstrap-select.min.js"></script>
 <script src="assets/bootstrap-touchspin/jquery.bootstrap-touchspin.min.js"></script>
  <script src="assets/tinymce/tinymce.min.js"></script>
    <script src="assets/bootstrap.file-input.js"></script>
    <script src="assets/jquery-validation-1.15.0/jquery.validate.min.js"></script>
   <script type="text/javascript">
        $('#txtPrice').TouchSpin({
            min: 0,
            max: 9999999,
            step: 10000,
            verticalbuttons: true,
        });
        $('#txtQ').TouchSpin({
            min: 1,
            verticalbuttons: true,
        });
        $('.selectpicker').selectpicker();

        tinymce.init({
            selector: 'textarea',
            menubar: false,
            toolbar1: 'styleselect | bold italic | link image | alignleft aligncenter',
            toolbar2: 'fontselect | fontsizeselect | forecolor backcolor',
            plugins: 'link image textcolor',
            encoding: 'xml',
        });
        $('input[type = file]').bootstrapFileInput();

        $.validator.addMethod("imageOnly", function (value, element) {
            return this.optional(element) || /^.+\.(jpg|JPG|png|PNG)$/.test(value);
        });
        $('#addProductForm').validate({
            rules: {
                CatName: {
                    required: true
                },
                CatID:{
					required: true	
                },
                fuMain: {
                    imageOnly: true
                },
                fuThumbs: {
                    imageOnly: true
                }
            },
            messages: {
                CatName: {
                    required: "Chưa nhập tên sản phẩm",
                },
                 CatID: {
                    required: "Chưa nhập rượu vang",
                },
                fuMain: {
                    imageOnly: "Hình lớn: chỉ chấp nhận file ảnh sản phẩm."
                },
                fuThumbs: {
                    imageOnly: "Hình nhỏ: chỉ chấp nhận file ảnh sản phẩm."
                }
            },
            errorElement: 'span',
            errorClass: 'help-block',

            highlight: function (element) {
                $(element)
                .closest('.form-group').addClass('has-error');
            },
            success: function (label) {
                label.closest('.form-group').removeClass('has-error');
                label.remove();
            },
        });

        function clearSeachBox() {
            $('#query'.val(''));
            $('#query'.focus(''));
        }
    </script>
JS;

 