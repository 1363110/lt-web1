<?php
require_once './inc_func.php';
require_once './dbHelper.php';

$flag = false;
 
 if(isset($_POST["btnThem"]))
 {
 	$ProName = $_POST["ProName"];
 	$CatName = $_POST["CatID"];
 	$wiID = $_POST["HangID"];
 	$Price =$_POST["Price"];
 	$Quantity = $_POST["Quantity"];
 	$TinyDes = $_POST["TinyDes"];
 	$FullDes =$_POST["FullDes"];

 	$sql ="INSERT into  products (ProName,CatID,wiID,Price,Quantity,TinyDes,FullDes) values('".$ProName."',".$CatName.",".$wiID.",".$Price.",".$Quantity.",'".$TinyDes."','".$FullDes."')";
 	save($sql, 0);
 }
 if(isset($_POST["btnXoa"]))
 {
 	$ProID = $_POST["ProID"];
 	$sql = "DELETE  from products where ProID =".$ProID;
 	save($sql,1);
 }

 if(isset($_POST["btnSua"]))
 {
 	$ProID = $_POST["ProID"];
 	$ProName = $_POST["ProName"];
 	$CatName = $_POST["CatID"];
 	$wiID = $_POST["HangID"];
 	$Price =$_POST["Price"];
 	$Quantity = $_POST["Quantity"];
 	$TinyDes = $_POST["TinyDes"];
 	$FullDes =$_POST["FullDes"];

 	$sql = "update products set ProName = '".$ProName."',CatID = ".$CatName.",wiID = ".$wiID.",Price = ".$Price.",Quantity = ".$Quantity.",TinyDes = '".$TinyDes."',FullDes = '".$FullDes."' where ProID = ".$ProID;
 	save($sql,1);
 }
$_ProID = 0;
$_ProName="";
$_CatID=0;
$_Price= 0;
$_TinyDes="";
$_Quantity=0;
$_FullDes="";
$wiID=0;
if(isset($_GET["ProID"]))
{
	$flag = true;
	$_ProID = $_GET["ProID"];
	$sql = "select * from products where ProID = ".$_ProID;
	$list = load($sql);
	$row = $list->fetch_assoc();


	$_ProID = $row["ProID"];
	$_ProName = $row["ProName"];
	$_CatID = $row["CatID"];
	$_Price = $row["Price"];
	$_TinyDes = $row["TinyDes"];
	$_Quantity = $row["Quantity"];
	$_FullDes = $row["FullDes"];
	$_wiID = $row["wiID"];
}



?>


<div class="col-md-9">
	<div class="panel panel-default">
		<div class="panel-heading">
			<h3 class="panel-title">Admin</h3>
		</div>
		<div class="panel-body">
			<table class="table table-hover" border="0">
				<thead>
					<tr class="bg-info">
						<th>STT</th>
						<th>Hình ảnh</th>
						<th width="160">Tên sản phẩm</th>
						<th>Giá sản pham</th>
						<th>Xuất xứ</th>
						<th>Loại sản  phẩm</th>
						<th>Số lượng</th>
						<th>Chức năng</th>

					</tr>
				</thead>
				<tbody>
					<?php
					$rowsPerPage = 10;
					$curPage = 1;
					if(isset($_GET["page"])) $curPage = $_GET["page"];
					$offset = ($curPage -1) * $rowsPerPage;

					$sql = "SELECT * FROM products inner join wine on products.wiID = wine.wiID inner join categories on products.CatID = categories.CatID order by ProID DESC LIMIT $offset, $rowsPerPage";
					$rs = load($sql);
					while($row = $rs->fetch_assoc())
					{
						?>
						<tr>
							<td><?php echo $row["ProID"];?></td>
							<td><img src="imgs/sp/<?php echo $row["ProID"]; ?>/main_thumbs.jpg" alt="..." width ="70"></td>
							<td><?php echo $row["ProName"];?></td>
							<td><?php echo number_format($row["Price"]);?></td>
							<td><?php echo $row["CatName"];?></td>
							<td><?php echo $row["WineName"];?></td>
							<td><?php echo $row["Quantity"];?></td>
							<td>
								<a  class="btn btn-primary" href="?act=admin&ProID=<?php echo $row["ProID"];?>&page=<?php echo $curPage?>">Chọn</a>
							</td>
						</tr>
						<?php
					}
					$sql = "select count(*) from products";

					$result = load($sql);
					$rows = $result->fetch_array();
					$number_of_rows = $rows[0];

					$number_of_pages = ceil($number_of_rows / $rowsPerPage);
					?>
					
				</tbody>
				<div class="col-md-9 ">
					<nav aria-label="Page navigation">
						<ul class="pagination">
							<?php
							if($curPage > 1){
								echo "
								<li class='previous'><a href='?act=admin&page=1''><span aria-hidden='true'>&larr;</span></a></li>
								<li>
									<a href='?act=admin&page=".($curPage-1)."' aria-label='Previous'>
										<span aria-hidden='true'>&laquo;</span>
									</a>
								</li>";
							}

							for($page = 1; $page <= $number_of_pages; $page++)
							{
								if($page == $curPage)
									echo "<li class='active'><a href='#'>".$page."</a></li>";
								else
									echo "<li><a href='?act=admin&page=".$page."'>".$page."</a></li>";
							}

							if($curPage < $number_of_pages )
							{
								echo "
								<li>
									<a href='?act=admin&page=".($curPage+1)."' aria-label='Next'>
										<span aria-hidden='true'>&raquo;</span>
									</a>
								</li>
								<li class='next'><a href='?act=admin&page=".$number_of_pages."''><span aria-hidden='true'>&rarr;</span></a></li>
								";
							}  
							?>      
						</ul>
					</nav>
				</div>
			</table>
			<hr>
			<!-- phần thêm xóa sửa -->

			<form class="form-horizontal" method="post" action="?act=admin" id="addProductForm" enctype="multipart/form-data">

				<input type="hidden" class="form-control" id="txtProID" name="ProID" value="<?php if($flag) echo $_ProID?>" />

				<div class="form-group">
					<label for="txtProName" class="col-sm-2 control-label">Sản phẩm:</label>
					<div class="col-sm-4">
						<input type="text" class="form-control" id="txtProName" name="ProName" value="<?php if($flag) echo $_ProName?>" />
					</div>
					<label for="CatID" class="col-sm-2 control-label">Rượu vang:</label>
					<div class="col-sm-4">
						<select class="form-control selectpicker" name="CatID">
						<option value="0">Chọn Rượu vang</option>
							<?php
							$sql = "select * from categories";
							$rs = load($sql);
							while($row = $rs->fetch_assoc())
							{
								?>
								<option <?php if($flag &&  $_CatID == $row["CatID"]) echo"selected";?> value="<?php echo $row["CatID"];?>"><?php echo $row["CatName"];?></option>
								<?php
							}
						?>
							</select>
						</div>      
					</div>
					<div class="form-group">
						<label for="HangID" class="col-sm-2 control-label">Loại vang</label>
						<div class="col-sm-10">
							<select class="form-control selectpicker" name="HangID">					
								<option value="0">Chọn loại vang</option>
							<?php
							$sql = "select * from wine";
							$rs = load($sql);
							while($row = $rs->fetch_assoc())
							{
								?>
								<option <?php if($flag &&  $_wiID == $row["wiID"]) echo"selected";?> value="<?php echo $row["wiID"];?>"><?php echo $row["WineName"];?></option>
								<?php
							}
						?>
							}
						</select>
					</div>
				</div>
				<div class="form-group">
					<label for="txtProName" class="col-sm-2 control-label">Giá:</label>
					<div class="col-sm-4">
						<input type="text" class="form-control" id="txtPrice" name="Price"  value="<?php if($flag) echo $_Price?>" />
					</div>
					<label for="txtQ" class="col-sm-2 control-label">Số lượng:</label>
					<div class="col-sm-4">
						<input type="text" class="form-control" id="txtQ" name="Quantity" value="<?php if($flag) echo $_Quantity?>" />
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label">Hình lớn:</label>
					<div class="col-sm-4">
						<input type="file" id="fuMain" name="fuMain" data-filename-placement="inside" />
					</div>
					<label class="col-sm-2 control-label">Hình nhỏ:</label>
					<div class="col-sm-4">
						<input type="file" id="fuThumbs" name="fuThumbs" @*data-filename-placement="inside"*@ />
					</div>
				</div>
				<div class="form-group">
					<label for="txtQ" class="col-sm-2 control-label">Mô tả Ngắn:</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" id="txtTinyDes" name="TinyDes" value="<?php if($flag) echo $_TinyDes?>" />
					</div>
				</div>
				<div class="form-group">
					<label for="txtQ" class="col-sm-2 control-label">Chi tiết:</label>
					<div class="col-sm-10 col-sm-offset-2">
						<textarea class="form-control" id="txtFullDes" name="FullDes" cols="5" rows="10">
							<?php if($flag) echo $_FullDes?>
						</textarea>
					</div>
				</div>
				<div class="col-sm-10 col-sm-offset-2">
					<?php if(!$flag) {?>
					<button type="submit" class="btn btn-primary" name="btnThem">
						<i class="fa fa-plus"></i>&nbsp;Thêm sản phẩm
					</button>
					<?php }else{ ?>
					<button type="submit" class="btn btn-success" name="btnSua">
						<i class="fa fa-edit"></i>&nbsp;Sửa sản phẩm
					</button>
					<button type="submit" class="btn btn-danger" name="btnXoa">
						<i class="fa fa-trash"></i>&nbsp;Xóa sản phẩm
					</button>
					
					<?php }?>
				</div>
			</form>


		</div>
	</div>
</div>
<?php
$js = <<<JS
	<script src="assets/bootstrap-select/js/bootstrap-select.min.js"></script>
 <script src="assets/bootstrap-touchspin/jquery.bootstrap-touchspin.min.js"></script>
  <script src="assets/tinymce/tinymce.min.js"></script>
    <script src="assets/bootstrap.file-input.js"></script>
    <script src="assets/jquery-validation-1.15.0/jquery.validate.min.js"></script>
   <script type="text/javascript">
        $('#txtPrice').TouchSpin({
            min: 0,
            max: 9999999,
            step: 10000,
            verticalbuttons: true,
        });
        $('#txtQ').TouchSpin({
            min: 1,
            verticalbuttons: true,
        });
        $('.selectpicker').selectpicker();

        tinymce.init({
            selector: 'textarea',
            menubar: false,
            toolbar1: 'styleselect | bold italic | link image | alignleft aligncenter',
            toolbar2: 'fontselect | fontsizeselect | forecolor backcolor',
            plugins: 'link image textcolor',
            encoding: 'xml',
        });
        $('input[type = file]').bootstrapFileInput();

        $.validator.addMethod("imageOnly", function (value, element) {
            return this.optional(element) || /^.+\.(jpg|JPG|png|PNG)$/.test(value);
        });
        $('#addProductForm').validate({
            rules: {
                ProName: {
                    required: true
                },
                CatID:{
					required: true	
                },
                fuMain: {
                    imageOnly: true
                },
                fuThumbs: {
                    imageOnly: true
                }
            },
            messages: {
                ProName: {
                    required: "Chưa nhập tên sản phẩm",
                },
                 CatID: {
                    required: "Chưa nhập rượu vang",
                },
                fuMain: {
                    imageOnly: "Hình lớn: chỉ chấp nhận file ảnh sản phẩm."
                },
                fuThumbs: {
                    imageOnly: "Hình nhỏ: chỉ chấp nhận file ảnh sản phẩm."
                }
            },
            errorElement: 'span',
            errorClass: 'help-block',

            highlight: function (element) {
                $(element)
                .closest('.form-group').addClass('has-error');
            },
            success: function (label) {
                label.closest('.form-group').removeClass('has-error');
                label.remove();
            },
        });

        function clearSeachBox() {
            $('#query'.val(''));
            $('#query'.focus(''));
        }
    </script>
JS;

 