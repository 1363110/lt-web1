<?php
require_once './dbHelper.php';
require_once './inc_func.php';

if (isset($_POST["btnLogin"])) {
    $uid = $_POST["txtUID"];
    $pwd = $_POST["txtPWD"];
    $enc_pwd = md5($pwd);

    $sql = "select * from users where f_Username = '$uid' and f_Password = '$enc_pwd'";
    $rs = load($sql);
    if ($rs->num_rows == 0) {
        $login_fail = 1;
    } else {

        $_SESSION["auth"] = 1;

        $row = $rs->fetch_assoc();
        $u = array();
        $u["f_Username"] = $row["f_Username"];
        $u["f_ID"] = $row["f_ID"];
        $u["f_Name"] = $row["f_Name"];
        $u["f_Email"] = $row["f_Email"];
        $u["f_DOB"] = $row["f_DOB"];
        $u["f_Permission"] = $row["f_Permission"];
        //print_r($u);
        $_SESSION["auth_user"] = $u;

        //$_SESSION["auth_username"] = $row["f_Username"];
        //$_SESSION["auth_id"] = $row["f_ID"];
        $remember = isset($_POST["chkRememberMe"]) ? true : false;
        if ($remember) {
            $expire = time() + 7 * 24 * 60 * 60;
            setcookie("auth_user_id", $row["f_ID"], $expire);
        }

        redirect("index.php");
    }
}

?>

<div class="col-md-9">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Đăng nhập</h3>
        </div>
        <div class="panel-body">
            <?php
            if(isAuthenticated() == true)
            {
                redirect("index.php");
            }
            ?>
            <form class="form-horizontal" action="" method="post" id="loginForm">

                <?php
                if (isset($login_fail) && $login_fail == 1) {
                    ?>
                    <div class="alert alert-warning alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <span>Đăng nhập thất bại</span>
                    </div>
                    <?php
                }
                ?>

                <div class="row">
                    <div class="col-md-10 col-md-offset-1 title">
                        Thông tin đăng nhập
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-5 col-md-offset-1">
                        <div class="form-group">
                            <div class="col-sm-12">                    
                                <input type="text" class="form-control" id="txtUID" name="txtUID" placeholder="Tên đăng nhập">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12">
                                <input type="password" class="form-control" id="txtPWD" name="txtPWD" placeholder="Mật khẩu">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-7">
                                <label style="font-weight: normal">
                                    <input type="checkbox" name="chkRememberMe" /> Ghi nhớ
                                </label>
                            </div>
                            <div class="col-sm-5">
                                <button type="submit" class="btn btn-primary pull-right" name="btnLogin" id="btnLogin">
                                    <i class="fa fa-check"></i> Đăng nhập
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>