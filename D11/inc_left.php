<div class="col-md-2">

<?php   if($_SESSION["auth_user"]["f_Permission"] == 1) {?>
     <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Chức năng</h3>
        </div>

        <div class="list-group">     
                <a class="list-group-item" href="index.php?act=admin"> THêm sản phẩm</a>
                <a class="list-group-item" href="index.php?act=category"> Thêm Danh mục rượu</a>
                <a class="list-group-item" href="index.php?act=wine1"> Thêm loại rượu</a>
        </div>
    </div>
    <?php }?>

    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Rượu Vang</h3>
        </div>

        <div class="list-group">
            <?php
            
            require_once './dbHelper.php';
            
            $sql = "select * from categories";
            $rs = load($sql);

            while ($row = $rs->fetch_assoc()) {
                $id = $row["CatID"];
                $name = $row["CatName"];
                ?>
                <a class="list-group-item" href="index.php?act=products&id=<?php echo $id; ?>"><?php echo $name; ?></a>
                <?php
            }
            ?>
        </div>
    </div>
 
     <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Loại Vang</h3>
        </div>

        <div class="list-group">
          <?php
            
            require_once './dbHelper.php';
            
            $sql = "select * from wine";
            $rs = load($sql);

            while ($row = $rs->fetch_assoc()) {
                $id = $row["wiID"];
                $name = $row["WineName"];
                ?>
                <a class="list-group-item" href="index.php?act=wine&id=<?php echo $id; ?>"><?php echo $name; ?></a>
                <?php
            }
            ?>
        </div>
    </div>

</div>
