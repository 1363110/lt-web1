<?php
require_once './inc_func.php';
require_once './dbHelper.php';

if (isset($_POST["btnAddToCart"])) {
  $sp = $_GET["id"];
  $slg = $_POST["txtQuantity"];
  setCart($sp, $slg);
  redirect("index.php?act=details&id=$sp");
}
?>

<div class="col-md-9">
  <div class="panel panel-default">
    <div class="panel-heading">
      <h3 class="panel-title">Chi tiết sản phẩm</h3>
    </div>
    <ol class="breadcrumb">
      <?php
      $id  = $_GET["id"];
      $sql ="SELECT products.*,categories.* from products,categories where products.CatID=categories.CatID AND products.Proid ='$id'";
      $rs = load($sql);
      $row = $rs->fetch_assoc();
      $CatID = $row["CatID"];
      
      ?>
      <li><a href="index.php"> <i class="fa fa-home"></i> Home</a></li>
      <li><a href="?act=products&id=<?php echo $CatID;?>"><?php echo $row["CatName"]; ?></a></li>
      <li class="active"><?php echo $row["ProName"]; ?></li>
    </ol>
    <div class="panel-body">
      <?php
      if (isset($_GET["id"])) {
        $id = $_GET["id"];
        $sql = "select * from products where ProID = $id";
        $rs = load($sql);
        if ($rs->num_rows == 0) {
          echo "KHÔNG CÓ SẢN PHẨM.";
        } else {
          $row = $rs->fetch_assoc();
          ?>
          <div class="row">
            <div class="col-md-12">
             <a href="imgs/sp/<?php echo $row["ProID"]; ?>/main.jpg" data-lightbox="<?php echo $row["ProID"]; ?>" data-title="<?php echo $row["ProName"]; ?>">
              <img src="imgs/sp/<?php echo $row["ProID"]; ?>/main.jpg"
              title="<?php echo $row["ProName"]; ?>"
              alt="<?php echo $row["ProName"]; ?>" />
            </a>
            
            
          </div>
          <div class="col-md-12 caption-lg">
            <?php echo $row["ProName"]; ?>
          </div>
          <div class="col-md-12">
            Giá: <span class="caption-sm"><?php echo number_format($row["Price"]); ?></span>
          </div>
          <div class="col-md-12">
            Lượt xem: <span class="caption-sm">
            <?php
                        //code lượt xem
            $counter = $row["View"];
            $new_count = $counter + 1; 
            $sql = "UPDATE products set View = $new_count where Proid = $id";
            save($sql,1);
            echo $counter;

            ?>
            
          </span>
        </div>
        <div class="col-md-12 padding">
          <?php echo $row["FullDes"]; ?>
        </div>
      </div>
      <?php if (isAuthenticated() == false || $row["Quantity"] <= 0) {
       

      }
      else{
        ?>
        <form class="form-horizontal" id="cartAdd-form" method="post" action="">
          <div class="form-group">
            <div class="col-sm-3">
              <div class="input-group" style="margin-left: 90px;">
                <input type="number" min ="1"  id="txtQuantity" name="txtQuantity" class="form-control" placeholder="Số lượng" value="1" style="width: 90px">
                <span class="input-group-btn">
                  <button class="btn btn-primary" type="submit" name="btnAddToCart">
                    <i class="fa fa-cart-plus"></i>
                  </button>
                </span>
              </div><!-- /input-group -->
            </div>
          </div>
        </form>
        <?php           
      }
    }
  } else {
    redirect("index.php");
  }
  ?>
</div>
<div class ="row">
  <div class="col-md-9 col-md-offset-1 title">

    Sản phẩm cùng loại
  </div>
</div>
<div class="row ">
  <?php
  $sql2="select * from products where CatID=$CatID and ProID<>'$id' order by rand() limit 0,6";
  $rs = load($sql2);
  while ($row = $rs->fetch_assoc()) {
    $id2=$row["ProID"];
    $tensp2=$row["ProName"]; 
    ?>
    <div class="col-xs-4 col-md-2 ">
      <a href="index.php?act=details&id=<?php echo $row["ProID"]; ?>" class="thumbnail">
        <img src="./imgs/sp/<?php echo $id2;?>/main_thumbs.jpg" alt="...">
      </a>
      <div class="caption">
        <?php echo $tensp2?>
      </div>
    </div>

    <?php
  }
  ?>
</div>
<div class ="row">
  <div class="col-md-9 col-md-offset-1 title">

    Sản phẩm cùng loại 2
  </div>
</div>
<div class="row ">
  <?php
   $sql ="SELECT products.*,wine.* from products,wine where products.wiID=wine.wiID AND products.Proid ='$id'";
      $rs = load($sql);
      $row = $rs->fetch_assoc();
      $wiID = $row["wiID"];

  $sql2="select * from products where wiID=$wiID and wiID<>'$id' order by rand() limit 0,6";
  $rs = load($sql2);
  while ($row = $rs->fetch_assoc()) {
    $id2=$row["ProID"];
    $tensp2=$row["ProName"]; 
    ?>
    <div class="col-xs-4 col-md-2 ">
      <a href="index.php?act=details&id=<?php echo $row["ProID"]; ?>" class="thumbnail">
        <img src="./imgs/sp/<?php echo $id2;?>/main_thumbs.jpg" alt="...">
      </a>
      <div class="caption">
        <?php echo $tensp2?>
      </div>
    </div>

    <?php
  }
  ?>
</div>

</div>
</div>
<?php
$js = '<script src="assets/lightbox2/js/lightbox.min.js" type="text/javascript"></script>';
?>