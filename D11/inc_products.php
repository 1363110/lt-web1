<?php
require_once './inc_func.php';
require_once './dbHelper.php';

if(isset($_POST["txtProId"])) {
  $sp = $_POST["txtProId"];
  $slg = 1;
  setCart($sp, $slg);
  $id = $_GET["id"];
  $curPage = $_GET["page"];
// header('Location:?act=products&id=' . $id . '&page=' . $curPage);
  if($curPage>1)
  redirect('?act=products&id='. $id . '&page=' . $curPage);
  else
  redirect('?act=products&id='. $id);
}

?>
<div class="col-md-9">

  <div class="panel panel-default">
    <div class="panel-heading">
      <h3 class="panel-title">Danh sách sản phẩm</h3>
    </div>
    <ol class="breadcrumb">
      <?php
      $id = $_GET["id"];
      $sql = "select * from categories where CatID = $id";
      $rs = load($sql);
      $row = $rs->fetch_assoc()
      ?>
      <li><a href="index.php"> <i class="fa fa-home"></i> Home</a></li>
      <li><a href="?act=products&id=<?php echo $id;?>"><?php echo $row["CatName"]; ?></a></li>
    </ol>
    <div class="panel-body">

      <?php
      $rowsPerPage = 4;
      $curPage = 1;
      if (isset($_GET["id"])) {
        if(isset($_GET["page"]))
          $curPage = $_GET["page"];
        $offset = ($curPage -1) * $rowsPerPage;
        $id = $_GET["id"];
        $sql = "select * from products where CatID = $id limit $offset, $rowsPerPage";
        $rs = load($sql);
        if ($rs->num_rows == 0) {
          echo "KHÔNG CÓ SẢN PHẨM.";
        } else {
          ?>
          <div class="row">
           <form id="f" action="" method="post">
            <input type="hidden" id="txtProId" name="txtProId" />
          </form>
          <?php
          while ($row = $rs->fetch_assoc()) {
            ?>
            <div class="col-sm-6">
              <div class="thumbnail">
               <a href="index.php?act=details&id=<?php echo $row["ProID"]; ?>">
                 <img src="imgs/sp/<?php echo $row["ProID"]; ?>/main_thumbs.jpg" alt="...">
               </a>

               <div class="caption">
                <h4><?php echo $row["ProName"]; ?></h4>
                <h4> Giá:<?php echo number_format($row["Price"]); ?> $</h4>
                <?php
                if($row["Quantity"] <= 0)
                {
                  ?>
                  <p><span class="label label-danger"> <i class="fa fa-ban"></i> Hết hàng</span></p>
                  <?php
                }
                else{
                  ?>
                  <p>
                   <span class="label label-success"> <i class="fa fa-cube">
                   </i> Số lượng: <?php echo $row["Quantity"]; ?>
                 </span></p>

               </p>
               <?php
             }
             ?>

             <p>
              <?php echo $row["TinyDes"]; ?>
            </p>
            <?php
            if (isAuthenticated() == false) {
              ?>
              <p>
               <a href="index.php?act=details&id=<?php echo $row["ProID"]; ?>" class="btn btn-primary" role="button">
                Chi tiết
              </a> 
              <?php
              if($row["Quantity"] <= 0 )
              {
                ?>
                <button class="btn btn-danger" type="button">
                 <i class="fa fa-ban"></i>
                 Hết hàng
               </button>
               <?php
             }
             ?>
           </p> 
           <?php
         } else {
           ?> 
           <p>
            <a href="index.php?act=details&id=<?php echo $row["ProID"]; ?>" class="btn btn-primary" role="button">
              Chi tiết
            </a> 
            <?php
            if($row["Quantity"] <= 0 )
            {
              ?>
              <button class="btn btn-danger" type="button">
               <i class="fa fa-ban"></i>
               Hết hàng
             </button>

             <?php
           }
           else{
            ?>
            <a href="#" class="btn btn-success" role="button" onclick="setProId(<?php echo $row["ProID"]; ?>);">
              <i class="fa fa-cart-plus"></i>
              Đặt hàng
            </a>
            <?php

          }
          ?>                             

        </p> 
        <?php
      }
      ?>                                    
    </div>
  </div>
</div>
<!-- phần phân trang -->

<?php
}

                    //end while


$sql = "select count(*) from products where CatID = $id ";

$result = load($sql);
$rows = $result->fetch_array();
$number_of_rows = $rows[0];

$number_of_pages = ceil($number_of_rows / $rowsPerPage);
?>

</div>
<div class="col-md-9 ">
  <nav aria-label="Page navigation">
    <ul class="pagination">
      <?php
      if($curPage > 1){
        echo "
        <li class='previous'><a href='?act=products&id=$id&page=1''><span aria-hidden='true'>&larr;</span> Đầu</a></li>
        <li>
        <a href='?act=products&id=$id&page=".($curPage-1)."' aria-label='Previous'>
          <span aria-hidden='true'>&laquo;</span>
        </a>
      </li>";
    }
    
    for($page = 1; $page <= $number_of_pages; $page++)
    {
      if($page == $curPage)
        echo "<li class='active'><a href='#'>".$page."</a></li>";
      else
        echo "<li><a href='?act=products&id=$id&page=".$page."'>".$page."</a></li>";
    }

    if($curPage < $number_of_pages )
    {
     echo "
     <li>
     <a href='?act=products&id=$id&page=".($curPage+1)."' aria-label='Next'>
      <span aria-hidden='true'>&raquo;</span>
    </a>
    </li>
    <li class='next'><a href='?act=products&id=$id&page=".$number_of_pages."''>Cuối <span aria-hidden='true'>&rarr;</span></a></li>
    ";
}  
?>      
</ul>
</nav>
</div>
<?php
//hết phần phân trang
}
}else {
  redirect("index.php");
}
?>
</div>
</div>
</div>

<?php
$js = <<<JS
<script type="text/javascript">
  function setProId(id) {
    f.txtProId.value = id;
    f.submit();
  }
</script>
JS;
?>