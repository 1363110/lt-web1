<?php
require_once './inc_func.php';
require_once './dbHelper.php';
if(isset($_POST["txtProId"])) {
  $sp = $_POST["txtProId"];
  $slg = 1;
  setCart($sp, $slg);
  redirect('index.php#');
}
?>            
<div class="col-md-10">
  <div class="panel panel-default">
    <div class="panel-heading">
      <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
        <!-- Indicators -->
        <ol class="carousel-indicators">
          <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
          <li data-target="#carousel-example-generic" data-slide-to="1"></li>
          <li data-target="#carousel-example-generic" data-slide-to="2"></li>
          <li data-target="#carousel-example-generic" data-slide-to="3"></li>
          <li data-target="#carousel-example-generic" data-slide-to="4"></li>
        </ol>
        <!-- Wrapper for slides -->
        <div class="carousel-inner" role="listbox">
          <div class="item active">
            <img src="imgs/logo1.png" alt="...">
            <div class="carousel-caption">
              <h3>Super <del>Fake</del>Cup</h3>
              <p>Hello! ♥</p>
            </div>
          </div>
          <div class="item">
            <img src="imgs/logo2.png" alt="...">
            <div class="carousel-caption">
              <h3>Chuyên </h3>
              <p>Bán các mặt hàng đa cấp</p>
            </div>
          </div>
          <div class="item">
            <img src="imgs/logo3.png" alt="...">
            <div class="carousel-caption">
              <h3>Cung Cấp</h3>
              <p>Hàng giả, nhái trất's lượng cao </p>
            </div>
          </div>
          <div class="item">
            <img src="imgs/logo4.png" alt="...">
            <div class="carousel-caption">
              <h3>Bạn muốn</h3>
              <p>Mua hàng fake ???</p>
            </div>
          </div>
          <div class="item">
            <img src="imgs/logo5.png" alt="...">
            <div class="carousel-caption">
              <h3> Đến SuperCup</h3>
              <p> web bán hàng đa cấp lớn nhất Việt Nam</p>
            </div>
          </div>
        </div>
        <!-- Controls -->
        <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
          <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
          <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>
      </div>
    </div>

    <div class="panel-body">
      <div>
        <!-- Nav tabs -->
        <ul class="nav nav-tabs" role="tablist">
          <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">sản phẩm Mới nhất</a></li>
          <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">sản phẩm bán chạy</a></li>
          <li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">sản phẩm nhiều lượt view</a></li>
          <li>
          </ul>
          <!-- Tab panes -->
          <div class="tab-content">

            <div role="tabpanel" class="tab-pane fade in active" id="home">
              <br>
              <form id="f" action="" method="post">
                <input type="hidden" id="txtProId" name="txtProId" />
              </form>
              <?php
              $sql = "select * from products ORDER BY ProID DESC LIMIT 0 , 10";
              $rs = load($sql);
              while ($row = $rs->fetch_assoc()) {
                ?>
                <div class="col-sm-6">
                  <div class="thumbnail">
                   <a href="index.php?act=details&id=<?php echo $row["ProID"]; ?>">
                     <img src="imgs/sp/<?php echo $row["ProID"]; ?>/main_thumbs.jpg" alt="...">
                   </a>                                 
                   <div class="caption">
                    <h4><?php echo $row["ProName"]; ?></h4>
                    <h4><?php echo number_format($row["Price"]); ?></h4>
                    <?php
                    if($row["Quantity"] <= 0)
                    {
                      ?>
                      <p><span class="label label-danger"> <i class="fa fa-ban"></i> Hết hàng</span></p>
                      <?php
                    }
                    else{
                      ?>
                      <p>
                       <span class="label label-success"> <i class="fa fa-cube">
                       </i> Số lượng: <?php echo $row["Quantity"]; ?>
                     </span></p>

                   </p>
                   <?php
                 }
                 ?>

                 <p>
                  <p>
                    <?php echo $row["TinyDes"]; ?>
                  </p>
                  <p>
                    <a href="index.php?act=details&id=<?php echo $row["ProID"]; ?>" class="btn btn-primary" role="button">
                      Chi tiết
                    </a>
                    <?php if(isAuthenticated()){
                      if($row["Quantity"] <= 0 )
                      {
                        ?>
                        <button class="btn btn-danger" type="button">
                         <i class="fa fa-ban"></i>
                         Hết hàng
                       </button>
                       <?php
                     }
                     else{
                       ?>

                       <a href="#" class="btn btn-success" role="button" onclick="setProId(<?php echo $row["ProID"]; ?>);">
                        <i class="fa fa-cart-plus"></i>
                        Đặt hàng
                      </a>
                      <?php
                    }
                  }
                  ?>
                </p>
              </div>
            </div>
          </div>
          <?php
        }
        ?>
      </div>
      <div role="tabpanel" class="tab-pane fade" id="profile">
        <br>
        <?php
        $sql = "select * from products pr, orderdetails dt where pr.proID = dt.ProID ORDER BY dt.Quantity DESC LIMIT 0 , 10";
        $rs = load($sql);
        while ($row = $rs->fetch_assoc()) {
          ?>
          <div class="col-sm-6">
            <div class="thumbnail">
              <a href="index.php?act=details&id=<?php echo $row["ProID"]; ?>">
               <img src="imgs/sp/<?php echo $row["ProID"]; ?>/main_thumbs.jpg" alt="...">
             </a>                                    
             <div class="caption">
              <h4><?php echo $row["ProName"]; ?></h4>
              <h4><?php echo number_format($row["Price"]); ?></h4>
              <?php
              if($row["Quantity"] <= 0)
              {
                ?>
                <p><span class="label label-danger"> <i class="fa fa-ban"></i> Hết hàng</span></p>
                <?php
              }
              else{
                ?>
                <p>
                 <span class="label label-success"> <i class="fa fa-cube">
                 </i> Số lượng: <?php echo $row["Quantity"]; ?>
               </span></p>

             </p>
             <?php
           }
           ?>

           <p>
            <p>
              <?php echo $row["TinyDes"]; ?>
            </p>
            <p>
              <a href="index.php?act=details&id=<?php echo $row["ProID"]; ?>" class="btn btn-primary" role="button">
                Chi tiết
              </a>
               <?php if(isAuthenticated()){
                      if($row["Quantity"] <= 0 )
                      {
                        ?>
                        <button class="btn btn-danger" type="button">
                         <i class="fa fa-ban"></i>
                         Hết hàng
                       </button>
                       <?php
                     }
                     else{
                       ?>

                       <a href="#" class="btn btn-success" role="button" onclick="setProId(<?php echo $row["ProID"]; ?>);">
                        <i class="fa fa-cart-plus"></i>
                        Đặt hàng
                      </a>
                      <?php
                    }
                  }
                  ?>     
            </p>
          </div>
        </div>
      </div>
      <?php
    }
    ?>
  </div>
  <div role="tabpanel" class="tab-pane fade" id="messages">
    <br>
     <?php
        $sql = "select * from products ORDER BY View DESC LIMIT 0 , 10";
        $rs = load($sql);
        while ($row = $rs->fetch_assoc()) {
          ?>
          <div class="col-sm-6">
            <div class="thumbnail">
              <a href="index.php?act=details&id=<?php echo $row["ProID"]; ?>">
               <img src="imgs/sp/<?php echo $row["ProID"]; ?>/main_thumbs.jpg" alt="...">
             </a>                                    
             <div class="caption">
              <h4><?php echo $row["ProName"]; ?></h4>
              <h4><?php echo number_format($row["Price"]); ?></h4>
              <?php
              if($row["Quantity"] <= 0)
              {
                ?>
                <p><span class="label label-danger"> <i class="fa fa-ban"></i> Hết hàng</span></p>
                <?php
              }
              else{
                ?>
                <p>
                 <span class="label label-success"> <i class="fa fa-cube">
                 </i> Số lượng: <?php echo $row["Quantity"]; ?>
               </span></p>

             </p>
             <?php
           }
           ?>

           <p>
            <p>
              <?php echo $row["TinyDes"]; ?>
            </p>
            <p>
              <a href="index.php?act=details&id=<?php echo $row["ProID"]; ?>" class="btn btn-primary" role="button">
                Chi tiết
              </a>
               <?php if(isAuthenticated()){
                      if($row["Quantity"] <= 0 )
                      {
                        ?>
                        <button class="btn btn-danger" type="button">
                         <i class="fa fa-ban"></i>
                         Hết hàng
                       </button>
                       <?php
                     }
                     else{
                       ?>

                       <a href="#" class="btn btn-success" role="button" onclick="setProId(<?php echo $row["ProID"]; ?>);">
                        <i class="fa fa-cart-plus"></i>
                        Đặt hàng
                      </a>
                      <?php
                    }
                  }
                  ?>     
            </p>
          </div>
        </div>
      </div>
      <?php
    }
    ?>
  </div>
</div>
</div>
</div>
</div>
</div>

<?php
$js = <<<JS
<script type="text/javascript">
  function setProId(id) {
    f.txtProId.value = id;
    f.submit();
  }
</script>
JS;
?>
