<?php
require_once './inc_func.php';
require_once'./dbHelper.php';

if (isAuthenticated() == false) {
  redirect("index.php?act=login");
}
?>

<div class="col-md-9">
  <div class="panel panel-default">
    <div class="panel-heading">
      <h3 class="panel-title">Thông tin cá nhân</h3>
    </div>
    <div class="panel-body">
      <?php
      $sql = "select * from users where f_ID ='".$_SESSION['auth_user']['f_ID']."'";
      if($rs = load($sql))
      {
       while ($row = $rs->fetch_assoc()) 
       {
        ?>
        <div class="row">
          <div class="col-md-10 col-md-offset-1 title">

            Thông tin cá nhân
          </div>
        </div>
        <div class="row">
          <div class="col-md-7 col-md-offset-1">
           <table class="table table-hover ">
            <tr>
              <tr > 
                <th scope="row" width="150">Tên đăng nhập</th> 
                <td><?php echo  $row["f_Username"]; ?></td> 
              </tr> 
              <tr> 
                <th scope="row">Họ tên </th> 
                <td> <?php echo  $row["f_Name"];?></td>           
              </tr>
              <tr> 
               <th scope="row">Email</th> 
               <td><?php echo  $row["f_Email"];?></td>                   
             </tr>                    
             <tr> 
              <th scope="row">Ngày sinh</th> 
              <td><?php echo  $row["f_DOB"];?></td>                  
            </tr>                     
          </table>
          <div class="col-sm-3 ">
            <a href="index.php?act=changeprofile&id=<?php echo $row['f_ID']; ?>" class="btn btn-primary" role="button">
               <i class="fa fa-edit"></i> Chỉnh sửa thông tin
              </a> 
          </div>
        </div>

      </div>
      <?php
    }
    $rs->free();
  }
  else
  {
   echo "No results found";
 }
 ?>

</div>

</div>

