<?php
require_once './dbHelper.php';
require_once './inc_func.php';

if (isset($_POST["btnRegister"])) {

    //
    // kiểm tra captcha

  if (empty($_SESSION['captcha']) || trim(strtolower($_POST['txtCaptcha'])) != $_SESSION['captcha']) {
   echo " 
   <div class='col-md-9'>
     <div class='alert alert-warning alert-dismissible' role='alert'>
      <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span>&times;</span></button>
      <span>Sai captcha</span>
    </div>
  </div>";
} else {
  $uid = $_POST["txtUID"];  
  $pwd = $_POST["txtPWD"];
  $enc_pwd = md5($pwd);

  $fullname = $_POST["txtFullName"];
  $email = $_POST["txtEmail"];
  $str_dob = $_POST["txtDOB"];
  $str_dob = str_replace('/', '-', $str_dob);
        $dob = strtotime($str_dob); //d-m-Y
        $str_dob = date('Y-m-d H:i:s', $dob);
        // echo $str_dob;

        $sql = "insert into users(f_Username, f_Password, f_Name, f_Email, f_DOB, f_Permission) values('$uid', '$enc_pwd', '$fullname', '$email', '$str_dob', 0)";
        $id = save($sql, 0);
        echo "
        <div class='col-md-9'>
         <div class='alert alert-success alert-dismissible' role='alert'>
          <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span>&times;</span></button>
          <span>Đăng ký thành công</span>
        </div>
      </div>
      ";     
    }
  }
  ?>

  <div class="col-md-9">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title">Đăng ký người dùng</h3>
      </div>
      <div class="panel-body">
        <form class="form-horizontal" action="" method="post" id="login-from1"">
          <div class="row">
            <div class="col-md-10 col-md-offset-1 title">
              Thông tin đăng nhập
            </div>
          </div>
          <div class="row">
            <div class="col-md-10 col-md-offset-1">
              <div class="form-group">
                <div class="col-sm-6">
                  <input type="text" class="form-control" name="txtUID" placeholder="Tên đăng nhập">
                </div>
              </div>
              <div class="form-group">
                <div class="col-sm-6">
                  <input type="password" class="form-control" name="txtPWD" placeholder="Mật khẩu">
                </div>
                <div class="col-sm-6">
                  <input type="password" class="form-control" name="txtConfirmPWD" placeholder="Nhập lại mật khẩu">
                </div>
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col-md-10 col-md-offset-1 title">
              Thông tin cá nhân
            </div>
          </div>
          <div class="row">
            <div class="col-md-10 col-md-offset-1">
              <div class="form-group">
                <div class="col-sm-6">
                  <input type="text" class="form-control" name="txtFullName" placeholder="Họ tên">
                </div>
              </div>
              <div class="form-group">
                <div class="col-sm-6">
                  <input type="email" class="form-control" name="txtEmail" placeholder="Email">
                </div>
                <div class="col-sm-6">                                                
                  <input type="text" class="form-control datepicker" name="txtDOB" placeholder="Ngày sinh">
                </div>
              </div>
              <div class="form-group">
                <div class="col-sm-6">
                  <img onclick="this.src = 'cool-php-captcha-0.3.1/captcha.php?' + Math.random();"  style="cursor: pointer" id="captchaImg" src="cool-php-captcha-0.3.1/captcha.php" />
                </div>
                <div class="col-sm-6">
                  <input type="text" class="form-control" id="txtCaptcha" name="txtCaptcha" placeholder="Captcha">
                </div>
              </div>
              <div class="form-group">
                <div class="col-sm-12">
                  <button type="submit" class="btn btn-success pull-right" name="btnRegister">
                    <i class="fa fa-check"></i> Đăng ký
                  </button>
                </div>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
<?php
$js = <<<JS
  <script src="assets/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
  <script src="assets/jquery-validation-1.15.0/jquery.validate.min.js" type="text/javascript"></script>
  <script type="text/javascript">
    $(function () {
      $('.datepicker').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true,
        startDate: '-3d'
      });
      $.validator.addMethod("vndate", function (value, element) {
        return this.optional(element) ||/^\d\d?\/\d\d?\/\d\d\d\d$/.test(value);
      });

    });


    $('#login-from1').validate
    ({
     onkeyup: false,
     onfocusout: false,
     rules: {
       txtUID: {
         required: true,
         minlength: 6
       },
       txtPWD: {
         required: true,
         minlength: 6
       },
       txtConfirmPWD: {
         required: true,
         equalTo: $("[name='txtPWD']")
       },
       txtFullName: {
         required: true,
       },
       txtEmail: {
         required: true,
         email: true
       },
       txtDOB: {
         required: true,
         vndate: true
       },
     },
     messages: {
       txtUID: {
         required:"Chưa nhập tên đăng nhập.",
         minlength:"Tên đăng nhập không được dưới 6 ký tự."
       },
       txtPWD: {
         required: "chưa nhập mật khẩu.",
         minlength:"mật khẩu phải nhiều hơn 6 ký tự."
       },
       txtConfirmPWD: {
         required: "Chưa nhập lại mật khẩu.",
         equalTo:"Mật khẩu nhập lại không khớp."
       },
       txtFullName: {
         required:"Chưa nhập họ tên.",
       },
       txtEmail: {
         required: "Chưa nhập email.",
         email:"Email không đúng định dạng."
       },
       txtDOB: {
         required: "Chưa nhập ngày sinh.",
         vndate:"Ngày sinh không đúng định dạng."
       },
     },

     errorElement: 'span',
     errorClass: 'help-block',

     highlight: function (element) {
       $(element)
       .closest('.form-group').addClass('has-error');
     },
     success: function (label) {
       label.closest('.form-group').removeClass('has-error');
     },
   });
 </script>

JS;
?>





 
