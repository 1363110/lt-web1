<?php
require_once './inc_func.php';
require_once './dbHelper.php';

if (isAuthenticated() == false) {
  redirect("index.php?act=login");
}
$user_id = $_SESSION["auth_user"]["f_ID"];
$sql = "SELECT * FROM users where f_ID = '$user_id'";
$rs = load($sql);
$row = $rs->fetch_assoc();
$db_id = $row["f_ID"];
?>
<?php
if(isset($_POST["btnChanghepass"]))
{
  $userID = $_POST["txtUserID"];
  $sql="SELECT * FROM users where f_ID ='$userID'";
  $rs2 = load($sql);
  $row2 = $rs2->fetch_assoc();
  $db_matkhau=$row2["f_Password"];

  $pw=$_POST['txtnPWD'];
  $pww=md5($pw);
  $pw_old=md5($_POST['txtoPWD']);
  if(strlen($pw)<6)
  echo "<script>alert('Mật khẩu mới phải lớn hơn 6 ký tự');</script>";
  else
  {
    if($pw_old == $db_matkhau)
    {
      $pw = md5($pw);
      $sql ="UPDATE users SET f_Password='$pww' WHERE f_ID='$userID'";
      save($sql,1);

    }
    else
      echo " 
   <div class='col-md-9'>
     <div class='alert alert-warning alert-dismissible' role='alert'>
      <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span>&times;</span></button>
      <span>Mật khẩu cũ không đúng!</span>
    </div>
  </div>";
  }

}
?>
<div class="col-md-9">
  <div class="panel panel-default">
    <div class="panel-heading">
      <h3 class="panel-title">Đổi mật khẩu</h3>
    </div>
    <div class="panel-body">
      <form class="form-horizontal" action="" method="post" id="changepassfrom">

        <?php
        if (save($sql,1) == false) {
        ?>
        <div class="alert alert-success alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <span>Đổi mật khẩu thành công vui lòng đăng xuất để vào lại!!</span>
      </div>
      <?php
    }
    ?>

    <div class="row">
      <div class="col-md-10 col-md-offset-1 title">
        Đổi mật khẩu cá nhân
      </div>
    </div>
    <div class="row">

      <div class="col-md-5 col-md-offset-1">
      <input type="hidden" class="form-control" id="txtoPWD" name="txtUserID" placeholder="Mật khẩu cũ" value="<?php echo $db_id;?>">
        <div class="form-group">
        <label class="col-sm-5 control-label">Mật khẩu hiện tại</label>
          <div class="col-sm-7">
            <input type="text" class="form-control" id="txtoPWD" name="txtoPWD" placeholder="Mật khẩu cũ">
          </div>
        </div>
        <div class="form-group">
        <label class="col-sm-5 control-label">Mật khẩu mới</label>
          <div class="col-sm-7">
            <input type="password" class="form-control" id="txtnPWD" name="txtnPWD" placeholder="Mật khẩu mới">
          </div>
        </div>
        <div class="form-group">
        <label class="col-sm-5 control-label">Nhập lại mật khẩu</label>
          <div class="col-sm-7">
            <input type="password" class="form-control" id="txtnPWD" name="txtrbPWD" placeholder="Nhập lại mật khẩu">
          </div>
        </div>
        <div class="form-group">
          <div class="col-sm-8">
            <button type="submit" class="btn btn-primary pull-right" name="btnChanghepass" id="btnChanghepass">
              <i class="fa fa-key"></i> Thay đổi
            </button>
          </div>
        </div>
      </div>
    </div>
  </form>
</div>
</div>
</div>
