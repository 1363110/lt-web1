<?php session_start();
error_reporting(0);
 ?>


<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
<head>
    <meta charset="UTF-8">
    <title>SuperCup</title>
    <link href="assets/bootstrap-3.3.4-dist/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="assets/font-awesome-4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
    <link href="assets/lightbox2/css/lightbox.min.css" rel="stylesheet" type="text/css"/>
    <link href="assets/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css"/>
    <link href="assets/BackTop/css/style.css" rel="stylesheet" type="text/css" >
    <link href="assets/bootstrap-select/css/bootstrap-select.min.css" rel="stylesheet" />
    <link href="assets/bootstrap-touchspin/jquery.bootstrap-touchspin.min.css" rel="stylesheet" />   
    <style type="text/css">
        .title {
            font-family: 'Open Sans',sans-serif;
            color:blue;
            font-size: 16pt;
            padding-top: 10px;
            padding-bottom: 10px;
            border-bottom: 3px solid #cccccc;
            margin-bottom: 35px;
        }
        body{background: #eee ;font-family: 'Open Sans', sans-serif;}
        h3{font-size: 30px; font-weight: 400;text-align: center;margin-top: 50px;}
        h3 i{color: #444;}

        .scroll-top-wrapper {
          position: fixed;
          right: 20px;
          bottom: 20px;
          opacity: 0.7;
          transition: opacity 0.15s ease-in-out;
      }
      .scroll-top-wrapper:hover {
          opacity: 1;
      }
  </style>
  <link rel="icon" href="imgs/W.png" type="image/x-icon" />
  <link rel="shortcut icon" href="imgs/W.png" type="image/x-icon" />
</head>
<body>
    <?php include_once "./inc_top.php"; ?>

    <br>
    <br>
    <br>
    <br>        

    <div class="container-fluid">
        <div class="row">
            <?php
            include_once './inc_left.php';

            if (isset($_GET["act"])) {
                $act = $_GET["act"];
                switch ($act) {
                    case "products":
                    include_once './inc_products.php';
                    break;
                    case "details":
                    include_once './inc_details.php';
                    break;

                    case "login":
                    include_once './inc_login.php';
                    break;
                    case "register":
                    include_once './inc_register.php';
                    break;
                    case "profile":
                    include_once './inc_profile.php';
                    break;
                    case "changepass":
                    include_once './inc_changepass.php';
                    break;
                    case "cart":
                    include_once  './inc_cart.php';
                    break;
                    case"changeprofile";
                    include_once  './inc_changeprofile.php';
                    break;
                    case"wine";
                    include_once  './inc_wine.php';
                    break;
                    case"admin";
                    include_once './admin/inc_admin.php';
                    break;
                    case"category";
                    include_once './admin/inc_categories.php';
                    break;
                    case"wine1";
                    include_once './admin/inc_wine.php';
                    break;
                    case"search";
                    include_once './inc_search.php';
                    break;
                    default:
                    include_once './inc_blank.php';
                    break;
                }
            } else {
                include_once './inc_blank.php';
            }
            ?>
        </div>
        <a href="javascript:" id="return-to-top"><i class="fa fa-chevron-up"></i></a>
    </div>
    <script src="assets/jquery-3.1.1.min.js" type="text/javascript"></script>
    <script src="assets/BackTop/js/index.js"></script>
    <script src="assets/bootstrap-3.3.4-dist/js/bootstrap.min.js" type="text/javascript"></script>
    
    <?php
    if (isset($js)) {
        echo $js;
    }
    ?>

</body>
</html>
